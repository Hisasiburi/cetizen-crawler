import time, requests
from datetime import datetime
import random

cetizen = "https://market.cetizen.com"

def get_html(url):
    r = requests.get(url)
    return r.text

def parse(html):
    collection = []
    raw0 = html.split("class='clr100'>")
    for ele in raw0[1:]:
        link = cetizen + ele.split("href='")[1].split("'>")[0]
        title = ele.split("</span></a></span>")[0].split(";'>")[-1].strip()
        price = ele.split("<span class=\"b ls-0\">")[1].split("</span></li>")[0]
        delivery = ele.split("<span class=\"p11   ls-0\">")[1].split("</span></li>")[0]
        status = ele.split("onmouseout=\"OnIconHide('")[4].split("</span>")[0].split('>')[-1]
        if not delivery.split(',')[0].isdigit():
            delivery = "0"
        collection.append((link, title.lower(), price, delivery, status.strip()))
    return collection

def main():
    already_seen=[]
    with open("./V30.list", "r", encoding='UTF-8') as f:
        for line in f:
            line = line.strip()
            if not line:
                continue
            already_seen.append(line)

    print("-- PREVIOUS --")
    for line in already_seen:
        print(line)
    print("--    NEW   --")
    while True:
        html1 = get_html("https://market.cetizen.com/market.php?q=market&auc_sale=1&escrow_motion=1&sc=1&qs=&auc_wireless=&auc_uid=6652&stype=&akeyword=&view_type=&just_one=&just_one_pcat=&just_one_name=&usim=1&asdate=1&dis_c=1&auc_price1=100000&auc_price2=300000&keyword_p=&pno=&pw=")
        html2 = get_html("https://market.cetizen.com/market.php?q=market&auc_sale=1&escrow_motion=1&sc=1&qs=&auc_wireless=&auc_uid=6650&stype=&akeyword=&view_type=&just_one=&just_one_pcat=&just_one_name=&usim=1&asdate=1&dis_c=1&auc_price1=100000&auc_price2=300000&keyword_p=&pno=&pw=")
        html3 = get_html("https://market.cetizen.com/market.php?q=market&auc_sale=1&escrow_motion=1&sc=1&qs=&auc_wireless=&auc_uid=6653&stype=&akeyword=&view_type=&just_one=&just_one_pcat=&just_one_name=&usim=1&asdate=1&dis_c=1&auc_price1=100000&auc_price2=300000&keyword_p=&pno=&pw=")
        col = parse(html1) + parse(html2) + parse(html3)
        detected = False
        for link, title, price, delivery, status in col:
            if title.find("") != -1 and title not in already_seen:
                already_seen.append(title)
                txt2num = lambda x: int(''.join(x.split(',')))
                print("[{}] {}\n  {}원(기기 {}원, 배송비 {}원), 상태 <{}> -> {}".format(datetime.now(), title, txt2num(price)+txt2num(delivery), price, delivery, status, link))
                detected = True
        if detected:
            detected = False
            with open("./V30.list", "w", encoding='UTF-8') as f:
                for title in already_seen:
                    f.write(title+'\n')
        time.sleep(60*15)

def kakaotalk():
    # template_object = {
    #     "object_type": "text",
    #     "text": "텍스트 영역입니다. 최대 200자 표시 가능합니다.",
    #     "link": {
    #         "web_url": "https://developers.kakao.com",
    #         "mobile_web_url": "https://developers.kakao.com"
    #     },
    #     "button_title": "바로 확인"
    # }
    # headers = {'Authorization': 'Bearer c81fd1c089171214ecfbad3a48007ee4'}
    # r = requests.post('https://kapi.kakao.com/v2/api/talk/memo/default/send', headers=headers, data = {'template_object':str(template_object)})
    # print(r.text)
    # r = requests.get("kauth.kakao.com/oauth/authorize?client_id=2065402363d6a3875f42f1f32c565161&redirect_uri=localhost&response_type=code")
    pass

if __name__=="__main__":
    main()

